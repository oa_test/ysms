package com.jesse.modules.job.task;

import com.jesse.modules.generator.dao.PFacCheckDao;
import com.jesse.modules.generator.entity.PFacCheckEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Component("checkFac")
public class checkFac implements ITask {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private PFacCheckDao checkDao;
    @Override
    public void run(String params) throws Exception {
        logger.debug("checkFac定时任务正在执行，参数为：{}", params);
        List<PFacCheckEntity> checkEntityList = checkDao.selectList(null);
        for (int i = 0; i<checkEntityList.size();i++){
            //上次检查时间LastcheckTime
            Date LastcheckTime = checkEntityList.get(i).getLastCheckDate();//取上次检查时间
            //下次检查时间NextcheckTime
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(LastcheckTime);
            calendar.add(calendar.DATE,checkEntityList.get(i).getCycDays()); //把日期往后增加一天,整数  往后推,负数往前移动
            Date NextcheckTime=calendar.getTime(); //这个时间就是日期计算后的结果

            SimpleDateFormat formatTime = new SimpleDateFormat("YYYY-MM-dd");

            String time2 = formatTime.format(NextcheckTime);
            //今天的日期
            String nowTime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            long m = sdf.parse(time2).getTime() - sdf.parse(nowTime).getTime();

           long remDays = ( m / (1000 * 60 * 60 * 24) );
           Integer remDays1 = Integer.parseInt(String.valueOf(remDays));

           PFacCheckEntity entity = new PFacCheckEntity();
           entity.setNextCheckTime(NextcheckTime);
           entity.setRemDays(remDays1);
           entity.setId(checkEntityList.get(i).getId());
           checkDao.updateById(entity);
        }

    }
}
