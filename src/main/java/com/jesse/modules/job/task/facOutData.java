package com.jesse.modules.job.task;
import com.jesse.common.utils.RedisUtils;
import com.jesse.modules.generator.dao.PFacFixDao;
import com.jesse.modules.generator.dao.PFacOutDao;
import com.jesse.modules.generator.vo.facDateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Component("facOutData")
public class facOutData implements ITask {

	@Autowired
	private PFacOutDao facOutDao;

	@Autowired
	private PFacFixDao facFixDao;

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Autowired
	private RedisUtils redisUtils;


	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run(String params){
		logger.debug("TestTask定时任务正在执行，参数为：{}", params);
		Calendar ca = Calendar.getInstance();
		ca.add(Calendar.DATE, -7);
		String month=(ca.get(Calendar.MONTH)+1)+"";
		String day=(ca.get(Calendar.DATE))+"";
		if(Integer.parseInt(month)<10){
			month="0"+month;
		}
		if(Integer.parseInt(day)<10){
			day="0"+day;
		}
		String time = ca.get(Calendar.YEAR)+(month)+day;
		List<facDateVO> list = facOutDao.select7day(time);
		List<facDateVO> listfix = facFixDao.select7day(time);
		int[] arr= new int[7];
		int[] arrfix= new int[7];
		for (int i=0;i<7;i++){
			int j = i;
			String finalTime = time;
			List<facDateVO> li = list.stream().filter(
					s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(finalTime)+ j)) ).collect(Collectors.toList());
			arr[i]=li.size();

			String finalTime1 = time;
			List<facDateVO> li2 = listfix.stream().filter(
					s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(finalTime1)+ j)) ).collect(Collectors.toList());
			arrfix[i]=li2.size();
		}
//		for(int a : arr){
//			System.out.println(a);
//		}
//		for(int a : arrfix){
//			System.out.println(a);
//		}
		redisUtils.set("indexLine",arr);
//		Map<String,Object> map = new HashMap<>();
//		map.put("a",1);
//		map.put("b",2);
//				redisTemplate.boundHashOps("hashkey").put(1,2);
		redisUtils.set("indexLinefix",arrfix);
	}
}
