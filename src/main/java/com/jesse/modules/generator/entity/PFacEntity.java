package com.jesse.modules.generator.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;

import com.jesse.modules.generator.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 设备详情
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("p_fac")
public class PFacEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@Excel(name = "设备ID")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 编号
	 */
	@Excel(name = "设备编号")
	private String facNum;
	/**
	 * 设备名称
	 */
	@Excel(name = "设备名称")
	private String name;
	/**
	 * 说明文档
	 */
	@Excel(name = "说明文档")
	private String guide;
	/**
	 * 图片
	 */
	@Excel(name = "图片")
	private String imageUrl;
	/**
	 * 类型:1采购，2捐赠
	 */
	@Excel(name = "类型")
	private Integer type;
	/**
	 * 操作员
	 */
	@Excel(name = "操作员")
	private String operator;
	/**
	 * 规格型号
	 */
	@Excel(name = "规格型号")
	private String model;
	/**
	 * 单位
	 */
	@Excel(name = "单位")
	private String unit;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 设备种类
	 */
	private String kind;
	/**
	 * 所属品牌
	 */
	@Excel(name = "所属品牌")
	private String brand;
	/**
	 * 在使用人
	 */
	@Excel(name = "在使用人")
	private String user;
	/**
	 * 放置地点
	 */
	@Excel(name = "放置地点")
	private String address;
	/**
	 * 使用次数
	 */
	@Excel(name = "使用次数")
	private Integer useCount;
	/**
	 * 状态 0空闲 1在借 2维修 3报废
	 */
	@Excel(name = "状态")
	private Integer status;
	/**
	 * 创建时间
	 */
	@Excel(name = "创建时间")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 修改时间
	 */
	@Excel(name = "修改时间")
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifiedTime;
	/**
	 * 购买部门
	 */
	private String buyDep;
	/**
	 * 生产厂家
	 */
	@Excel(name = "生产厂家")
	private Integer made;
	/**
	 * 产地
	 */
	@Excel(name = "产地")
	private String madeIn;
	/**
	 * 出厂日期
	 */
	@Excel(name = "出厂日期")
	@JSONField(format = "yyyy-MM-dd")
	private Date madeOutTime;
	/**
	 * 购买时间
	 */
	@Excel(name = "购买时间")
	@JSONField(format = "yyyy-MM-dd")
	private Date expTime;
	/**
	 * 购买价格
	 */
	@Excel(name = "购买价格")
	private BigDecimal price;
	/**
	 * 启用时间
	 */
	@Excel(name = "启用时间")
	@JSONField(format = "yyyy-MM-dd")
	private Date enableTime;
	/**
	 * 备注
	 */
	@Excel(name = "备注")
	private String remark;

	/**
	 * 联系电话
	 */
	@Excel(name = "联系电话")
	private String phone;
	/**
	 * 1级分类
	 */
	private Long oneCategoryId;
	/**
	 * 2级分类
	 */
	private Long twoCategoryId;
	/**
	 * 3级分类
	 */
	private Long threeCategoryId;
	/*
	*报废标识
	 */
	@TableLogic
	private int deleted;

}
