package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 设备报废登记表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("p_fac_dpcd")
public class PFacDpcdEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 编号
	 */
	private String facNum;
	/**
	 * 设备名称
	 */
	private String name;
	/**
	 * 图片
	 */
	private String imageUrl;
	/**
	 * 操作员
	 */
	private String operator;
	/**
	 * 规格型号
	 */
	private String model;
	/**
	 * 单位
	 */
	private String unit;
	/**
	 * 设备种类
	 */
	private String kind;
	/**
	 * 状态 0空闲 1在借 2维修 3报废
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifiedTime;
	/**
	 * 购买部门
	 */
	private String buyDep;
	/**
	 * 报废时间
	 */
	private Date dpcdTime;
	/**
	 * 报废原因
	 */
	private String resn;
	/**
	 * 备注
	 */
	private String remark;

}
