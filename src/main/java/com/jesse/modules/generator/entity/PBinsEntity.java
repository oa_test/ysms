package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 生产厂家信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Data
@TableName("p_bins")
public class PBinsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long id;
	/**
	 * 编号
	 */
	private String num;
	/**
	 * 厂家名称
	 */
	private String name;
	/**
	 * 图片
	 */
	private String imageUrl;
	/**
	 * 厂家地址
	 */
	private String address;
	/**
	 * 状态 0空闲 1在借 2维修 3报废
	 */
	private Integer status;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 备注
	 */
	private String remark;

}
