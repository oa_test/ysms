package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 直播
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-03-16 11:03:22
 */
@Data
@TableName("p_live")
public class PLiveEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 序号
	 */
	@TableId
	private Integer id;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 直播地址
	 */
	private String liveUrl;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 直播描述
	 */
	private String remark;
	/**
	 * 0有效，1无效
	 */
	private Integer status;

}
