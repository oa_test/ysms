package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-03-27 17:17:28
 */
@Data
@TableName("p_fac_renew_log")
public class PFacRenewLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Integer id;
	/**
	 *
	 */
	private String facNum;
	/**
	 *
	 */
	private String renewRemark;
	/**
	 *
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 *
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifiedTime;
	/**
	 *
	 */
	private Integer renewCount;
	/**
	 * 新的归还时间
	 */
	private Date newTime;
	/**
	 * 旧的归还时间
	 */
	private Date oldTime;
	/**
	 * 使用人续借人
	 */
	private String user;
	private String imageUrl;

}
