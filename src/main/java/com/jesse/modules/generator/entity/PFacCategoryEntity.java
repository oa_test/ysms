package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 分类
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("p_fac_category")
public class PFacCategoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 类别id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 类别名称
	 */
	private String name;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 *
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 *
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifiedTime;
	/**
	 * 父级分类id
	 */
	private Long pid;

	@TableField(exist = false)
	private List<PFacCategoryEntity> children;

}
