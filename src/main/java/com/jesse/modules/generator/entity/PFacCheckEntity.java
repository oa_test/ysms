package com.jesse.modules.generator.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import com.jesse.modules.generator.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("p_fac_check")
public class PFacCheckEntity implements Serializable  {
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Excel(name = "设备ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 编号
     */
    @Excel(name = "设备编号")
    private String facNum;

    @Excel(name = "设备名称")
    private  String name;

    /*
     * 上次检查日期
     */
    @Excel(name="上次检查日期")
    private Date lastCheckDate;

    /*定期检查周期(天数)
	 */
    @Excel(name="检查周期")
    private Integer cycDays;

    /*
     * 剩余检查天数
     */
    @Excel(name="剩余检查天数")
    private Integer remDays;

    /*
    *下次检查时间
     */
    @Excel(name = "下次检查时间")
    private Date NextCheckTime;

    /**
     * 创建时间
     */

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改时间
     */

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date modifiedTime;

    private String remark;

}
