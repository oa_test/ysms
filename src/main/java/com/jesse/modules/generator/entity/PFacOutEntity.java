package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.jesse.modules.generator.annotation.Excel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 设备使用
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("p_fac_out")
public class PFacOutEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@Excel(name = "设备ID")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 编号
	 */
	@Excel(name = "设备编号")
	private String facNum;
	/**
	 * 设备名称
	 */
	@Excel(name = "设备名称")
	private String name;
	/**
	 * 使用人
	 */
	@Excel(name = "在借人")
	private String user;
	/**
	 * 使用所属部门
	 */
	private String dep;
	/**
	 * 图片
	 */
	private String imageUrl;
	/**
	 * 使用开始时间
	 */
	@Excel(name = "开始使用时间")
	private Date outTime;
	/**
	 * 实际归还时间
	 */
	@Excel(name = "归还时间")
	private Date retnTime;
	/**
	 * 预计归还时间
	 */
	@Excel(name = "预计归还时间")
	private Date nextTime1;
	/**
	 * 时间2
	 */
	@Excel(name = "预计归还时间")
	private Date nextTime2;
	/**
	 * 时间3
	 */
	private Date nextTime3;
	/**
	 * 备注
	 */
	@Excel(name = "备注")
	private String remark;
	/**
	 * 规格型号
	 */
	private String model;
	/**
	 * 单位
	 */
	private String unit;
	/**
	 * 设备种类
	 */
	private String kind;
	/**
	 * 状态
	 */
	@Excel(name = "状态")
	private Integer status;
	/**
	 * 创建时间
	 */
	@Excel(name = "创建时间")
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifiedTime;

}
