package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 设备维修记录
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Data
@TableName("p_fac_fix")
public class PFacFixEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long id;
	/**
	 * 编号
	 */
	private String facNum;
	/**
	 * 设备名称
	 */
	private String name;
	/**
	 * 图片
	 */
	private String imageUrl;
	/**
	 * 规格型号
	 */
	private String model;
	/**
	 * 单位
	 */
	private String unit;
	/**
	 * 设备种类
	 */
	private String kind;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 维修人
	 */
	private String fixUser;
	/**
	 * 维修工时
	 */
	private Integer fixTime;
	/**
	 * 维修费用
	 */
	private BigDecimal price;
	/**
	 * 故障原因
	 */
	private String fixResn;
	/**
	 * 开始时间
	 */
	private Date fixOutTime;
	/**
	 * 结束时间
	 */
	private Date fixInTime;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifiedTime;
	/**
	 * 备注
	 */
	private String remark;

}
