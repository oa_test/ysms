package com.jesse.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 设备资料
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("p_fac_info")
public class PFacInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;
	/**
	 * 编号
	 */
	private String facNum;
	/**
	 * 设备名称
	 */
	private String name;
	/**
	 * 图片
	 */
	private String imageUrl;
	/**
	 * 单位
	 */
	private String unit;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 设备种类
	 */
	private String kind;
	/**
	 * 数量
	 */
	private Integer num;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date modifiedTime;
	/**
	 * 1级分类
	 */
	private Long oneCategoryId;
	/**
	 * 2级分类
	 */
	private Long twoCategoryId;
	/**
	 * 3级分类
	 */
	private Long threeCategoryId;
	/**
	 * 备注
	 */
	private String remark;

}
