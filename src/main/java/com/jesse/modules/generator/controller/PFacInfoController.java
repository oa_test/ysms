package com.jesse.modules.generator.controller;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;
import com.jesse.modules.generator.dao.PFacCategoryDao;
import com.jesse.modules.generator.dao.PFacInfoDao;
import com.jesse.modules.generator.entity.PFacInfoEntity;
import com.jesse.modules.generator.service.PFacInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 设备资料
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@RestController
@RequestMapping("generator/pfacinfo")
public class PFacInfoController {

    @Autowired
    private PFacInfoService pFacInfoService;
    @Autowired
    private PFacCategoryDao categoryDao;
    @Autowired
    private PFacInfoDao facInfoDao;


    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:pfacinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pFacInfoService.queryPage(params);
//        System.out.println(JSON.parseObject(JSON.toJSONString(page.getList().get(0))).get("oneCategoryId"));
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pfacinfo:info")
    public R info(@PathVariable("id") Long id){
		PFacInfoEntity pFacInfo = pFacInfoService.getById(id);
        return R.ok().put("pFacInfo", pFacInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:pfacinfo:save")
    public R save(@RequestBody PFacInfoEntity pFacInfo){
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("name",pFacInfo.getName());
        PFacInfoEntity facInfoEntity = facInfoDao.selectOne(wrapper);
        if (facInfoEntity == null){
            pFacInfoService.save(pFacInfo);
            return R.ok();
        }
        return R.error(99,"名称已经存在");

    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:pfacinfo:update")
    public R update(@RequestBody PFacInfoEntity pFacInfo){
		pFacInfoService.updateById(pFacInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:pfacinfo:delete")
    public R delete(@RequestBody Long[] ids){
		pFacInfoService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
