package com.jesse.modules.generator.controller;

import java.util.Arrays;
import java.util.List;

import com.jesse.common.annotation.SysLog;
import com.jesse.common.utils.R;
import com.jesse.modules.generator.dao.PFacCategoryDao;
import com.jesse.modules.generator.entity.PFacCategoryEntity;
import com.jesse.modules.generator.service.PFacCategoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 分类
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@RestController
@RequestMapping("generator/pfaccategory")
public class PFacCategoryController {
    @Autowired
    private PFacCategoryService pFacCategoryService;

    @Autowired
    private PFacCategoryDao categoryDao;


    /**
     * 查出所有分类子分类  树形组装
     */
    @RequestMapping("/list/tree")
    @RequiresPermissions("generator:pfaccategory:list")
    public R list(){
        List<PFacCategoryEntity> entities = pFacCategoryService.listWithTree();
        return R.ok().put("data", entities);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pfaccategory:info")
    public R info(@PathVariable("id") Long id){
		PFacCategoryEntity pFacCategory = pFacCategoryService.getById(id);

        return R.ok().put("pFacCategory", pFacCategory);
    }

    /**
     * 保存
     */
    @SysLog("添加分类")
    @RequestMapping("/save")
    @RequiresPermissions("generator:pfaccategory:save")
    public R save(@RequestBody PFacCategoryEntity pFacCategory){
		pFacCategoryService.save(pFacCategory);

        return R.ok();
    }

    /**
     * 修改
     */
    @SysLog("修改分类")
    @RequestMapping("/update")
    @RequiresPermissions("generator:pfaccategory:update")
    public R update(@RequestBody PFacCategoryEntity pFacCategory){
		pFacCategoryService.updateById(pFacCategory);

        return R.ok();
    }

    /**
     * 删除
     */
    @SysLog("删除分类")
    @RequestMapping("/delete")
    @RequiresPermissions("generator:pfaccategory:delete")
    public R delete(@RequestBody Long[] ids){
		pFacCategoryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }


}
