package com.jesse.modules.generator.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jesse.common.annotation.SysLog;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;
import com.jesse.common.utils.poi.ExcelUtil;
import com.jesse.modules.generator.dao.PFacDao;
import com.jesse.modules.generator.entity.PFacEntity;
import com.jesse.modules.generator.entity.PFacOutEntity;
import com.jesse.modules.generator.service.PFacOutService;
import com.jesse.modules.generator.dao.PFacOutDao;
import com.jesse.modules.generator.service.PFacRenewLogService;
import com.jesse.modules.generator.service.PFacService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 设备使用
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@RestController
@RequestMapping("generator/pfacout")
public class PFacOutController {
    @Autowired
    private PFacOutService pFacOutService;
    @Autowired
    private PFacService pFacService;
    @Autowired
    private PFacDao facDao;
    @Autowired
    private PFacOutDao facOutDao;
    @Autowired
    private PFacRenewLogService pFacRenewLogService;


    /**
     * 列表
     */
    @RequestMapping("/a_list")
    @RequiresPermissions("generator:pfacout:list")
    public R auserlist(@RequestParam Map<String, Object> params){
        PageUtils page = pFacOutService.aQueryPage(params);
        return R.ok().put("page", page);
    }
    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:pfacout:list")
    public R list(@RequestParam Map<String, Object> params){
        System.out.println(params.toString());
        PageUtils page = pFacOutService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pfacout:info")
    public R info(@PathVariable("id") Long id){
		PFacOutEntity pFacOut = pFacOutService.getById(id);
        return R.ok().put("pFacOut", pFacOut);
    }

    /**
     * 保存
     */
    @SysLog("设备借用")
    @RequestMapping("/save")
    @RequiresPermissions("generator:pfacout:save")
    public R save(@RequestBody PFacOutEntity pFacOut){
		pFacOutService.save(pFacOut);
        QueryWrapper<PFacEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("fac_num",pFacOut.getFacNum());
        PFacEntity facEntity = facDao.selectOne(wrapper);
        int a = facEntity.getUseCount()+1;
        facEntity.setUseCount(a);
        facEntity.setStatus(4);
        facEntity.setUser(pFacOut.getUser());
        facDao.updateById(facEntity);
        return R.ok();
    }

    /**
     * 修改
     */
    @SysLog("续借")
    @RequestMapping("/update")
    @RequiresPermissions("generator:pfacout:update")
    public R update(@RequestBody PFacOutEntity pFacOut){

        pFacOutService.renewOutFac(pFacOut);

        return R.ok();
    }

    /**
     * 归还
     */
    @SysLog("借用归还")
    @RequestMapping("/rentfac")
    @RequiresPermissions("generator:pfacout:update")
    public R delete(@RequestBody String id){
        String str =id.replace("\"", "");
        PFacOutEntity facOutEntity = facOutDao.selectById(Integer.parseInt(str));
        facOutEntity.setStatus(4);
        facOutEntity.setRetnTime( new Timestamp(new Date().getTime()));
        facOutDao.updateById(facOutEntity);
        facDao.changeToUseTo7(facOutEntity.getFacNum());
        return R.ok();
    }
    /*
    *撤销申请
     */
    @SysLog("撤销预约申请")
    @RequestMapping("/undofac")
    @RequiresPermissions("generator:pfacout:update")
    public R undofac(@RequestBody String id){
        String str =id.replace("\"", "");
        PFacOutEntity facOutEntity = facOutDao.selectById(Integer.parseInt(str));
        facDao.changeToUseTo0(facOutEntity.getFacNum());
        facOutDao.deleteById(facOutEntity);
        return R.ok();
    }

    /*
    *查询续借记录
     */
    @RequestMapping("/getRenewListAll")
    @RequiresPermissions("generator:pfacout:list")
    public R getRenewListAll(@RequestParam Map<String, Object> params){
        PageUtils page = pFacRenewLogService.getRenewListAll(params);
        return R.ok().put("page", page);
    }

    /*
     *查询续借记录
     */
    @RequestMapping("/getRenewListUser")
    @RequiresPermissions("generator:pfacout:list")
    public R getRenewListUser(@RequestParam Map<String, Object> params){
        PageUtils page = pFacRenewLogService.getRenewListUser(params);
        return R.ok().put("page", page);
    }

    @GetMapping("/export")
    public R export(PFacEntity pFacEntity)
    {
        List<PFacOutEntity> list = facOutDao.selectList(null);
        ExcelUtil<PFacOutEntity> util = new ExcelUtil<PFacOutEntity>(PFacOutEntity.class);
        return util.exportExcel(list, "pFacEntity");
    }



}
