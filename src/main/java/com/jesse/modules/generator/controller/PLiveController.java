package com.jesse.modules.generator.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jesse.modules.generator.entity.PLiveEntity;
import com.jesse.modules.generator.service.PLiveService;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;



/**
 * 直播
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-03-16 11:03:22
 */
@RestController
@RequestMapping("generator/plive")
public class PLiveController {
    @Autowired
    private PLiveService pLiveService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:plive:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pLiveService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:plive:info")
    public R info(@PathVariable("id") Integer id){
		PLiveEntity pLive = pLiveService.getById(id);

        return R.ok().put("pLive", pLive);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:plive:save")
    public R save(@RequestBody PLiveEntity pLive){
		pLiveService.save(pLive);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:plive:update")
    public R update(@RequestBody PLiveEntity pLive){
		pLiveService.updateById(pLive);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:plive:delete")
    public R delete(@RequestBody Integer[] ids){
		pLiveService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
