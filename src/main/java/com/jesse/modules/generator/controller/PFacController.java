package com.jesse.modules.generator.controller;

import java.text.ParseException;
import java.util.*;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jesse.common.annotation.SysLog;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;
import com.jesse.common.utils.poi.ExcelUtil;
import com.jesse.modules.generator.dao.PFacCheckDao;
import com.jesse.modules.generator.dao.PFacDao;
import com.jesse.modules.generator.entity.PFacCheckEntity;
import com.jesse.modules.generator.vo.PFacVO;
import com.jesse.modules.generator.dao.PFacOutDao;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jesse.modules.generator.entity.PFacEntity;
import com.jesse.modules.generator.service.PFacService;
import com.alibaba.fastjson.JSON;
/**
 * 设备详情
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@RestController
@RequestMapping("generator/pfac")
public class PFacController {
    @Autowired
    private PFacService pFacService;
    @Autowired
    private PFacDao pFacDao;
    @Autowired
    private PFacOutDao facOutDao;
    @Autowired
    private PFacCheckDao facCheckDao;
    @Autowired
    private PFacCheckDao checkDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:pfac:list")
    public R list(@RequestParam Map<String, Object> params){
        System.out.println(params.toString());
        PageUtils page = pFacService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pfac:info")
    public R info(@PathVariable("id") Long id){
		PFacEntity pFac = pFacService.getById(id);

        return R.ok().put("pFac", pFac);
    }

    /**
     * 保存
     */
    @SysLog("添加入库")
    @RequestMapping("/save")
    @RequiresPermissions("generator:pfac:save")
    public R save(@RequestBody Map<String, Object> params) throws ParseException {
        //
        String  str =  JSONArray.toJSONString(params.get("pFac"));
        PFacEntity pFacEntity2 = JSON.parseObject(str,PFacEntity.class);
        PFacEntity pFacEntity = pFacService.isExist(pFacEntity2);

        if(pFacEntity!=null){
            return R.error(99,"设备编号已经存在");
        }
        if(pFacEntity==null){
            int res = pFacService.saveFac(params);
            if (res==0){
                return R.ok();
            }
        }
        return R.error();
    }
    /**
     * 修改
     */
    @SysLog("修改/维护设备")
    @RequestMapping("/update")
    @RequiresPermissions("generator:pfac:update")
    public R update(@RequestBody PFacEntity pFac){
		pFacService.updateById(pFac);

        return R.ok();
    }

    /**
     * 删除
     */
    @SysLog("删除设备")
    @RequestMapping("/delete")
    @RequiresPermissions("generator:pfac:delete")
    public R delete(@RequestBody Long[] ids){
		pFacService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }
    /**
     * 预约审核通过
     */
    @SysLog("借用审核预约")
    @RequestMapping("/passaudit/{id}")
    @RequiresPermissions("generator:pfac:audit")
    public R passaudit(@PathVariable("id") Long id,
    @RequestParam Map<String, Object> params){
        pFacDao.passaudit(id);
        facOutDao.passaudit(params.get("facout").toString());
        return R.ok();
    }
    /**
     * 预约审核拒绝
     */
    @SysLog("拒绝借用预约")
    @RequestMapping("/refusaudit/{id}")
    @RequiresPermissions("generator:pfac:audit")
    public R refusaudit(@PathVariable("id") Long id,
    @RequestParam Map<String, Object> params){
        PFacEntity facEntity = pFacDao.selectById(id);
        facEntity.setStatus(0);
        facEntity.setUser("");
        pFacDao.updateById(facEntity);
        facOutDao.refusaudit(params.get("facout").toString());
        return R.ok();
    }

    /**
     * 归还审核通过
     */
    @SysLog("归还审核")
    @RequestMapping("/passRtnAudit/{id}")
    @RequiresPermissions("generator:pfac:audit")
    public R passRtnAudit(@PathVariable("id") Long id,
                        @RequestParam Map<String, Object> params){
        PFacEntity facEntity = pFacDao.selectById(id);
        facEntity.setStatus(0);
        facEntity.setUser("");
        pFacDao.updateById(facEntity);
//        pFacDao.passRtnAudit(id);
        facOutDao.passRtnAudit(params.get("facout").toString());
        return R.ok();
    }

    @RequestMapping("/findFacList")
    @RequiresPermissions("generator:pfac:list")
    @ResponseBody
    public R findFacList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                         @RequestParam(value = "pageSize") Integer pageSize,
                         @RequestParam(value = "categorys", required = false) String categorys,
                         PFacVO pFacVO){
        pFacVO.setStatus(0);
        Page<PFacEntity> facVOList =pFacService.facVOList(pageNum, pageSize, pFacVO);
        return R.ok().put("page",facVOList);
    }

    @GetMapping("/export")
    public R export(PFacEntity pFacEntity)
    {
        List<PFacEntity> list = pFacDao.selectList(null);
        ExcelUtil<PFacEntity> util = new ExcelUtil<PFacEntity>(PFacEntity.class);
        return util.exportExcel(list, "pFacEntity");
    }

}
