package com.jesse.modules.generator.controller;

import java.util.Arrays;
import java.util.Map;

import com.jesse.common.annotation.SysLog;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;
import com.jesse.modules.generator.entity.PBinsEntity;
import com.jesse.modules.generator.service.PBinsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 生产厂家信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@RestController
@RequestMapping("generator/pbins")
public class PBinsController {
    @Autowired
    private PBinsService pBinsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:pbins:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pBinsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pbins:info")
    public R info(@PathVariable("id") Long id){
		PBinsEntity pBins = pBinsService.getById(id);

        return R.ok().put("pBins", pBins);
    }

    /**
     * 保存
     */
    @SysLog("添加来源信息")
    @RequestMapping("/save")
    @RequiresPermissions("generator:pbins:save")
    public R save(@RequestBody PBinsEntity pBins){
        pBins.setStatus(0);
		pBinsService.save(pBins);

        return R.ok();
    }

    /**
     * 修改
     */
    @SysLog("修改来源信息")
    @RequestMapping("/update")
    @RequiresPermissions("generator:pbins:update")
    public R update(@RequestBody PBinsEntity pBins){
		pBinsService.updateById(pBins);

        return R.ok();
    }

    /**
     * 删除
     */
    @SysLog("删除来源信息")
    @RequestMapping("/delete")
    @RequiresPermissions("generator:pbins:delete")
    public R delete(@RequestBody Long[] ids){
		pBinsService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
