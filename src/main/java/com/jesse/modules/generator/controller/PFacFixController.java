package com.jesse.modules.generator.controller;

import java.util.Arrays;
import java.util.Map;

import com.jesse.common.annotation.SysLog;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;
import com.jesse.modules.generator.dao.PFacDao;
import com.jesse.modules.generator.dao.PFacFixDao;
import com.jesse.modules.generator.entity.PFacFixEntity;
import com.jesse.modules.generator.service.PFacFixService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 设备维修记录
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@RestController
@RequestMapping("generator/pfacfix")
public class PFacFixController {
    @Autowired
    private PFacFixService pFacFixService;
    @Autowired
    private PFacDao pFacDao;
    @Autowired
    private PFacFixDao facFixDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:pfacfix:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pFacFixService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pfacfix:info")
    public R info(@PathVariable("id") Long id){
		PFacFixEntity pFacFix = pFacFixService.getById(id);

        return R.ok().put("pFacFix", pFacFix);
    }

    /**
     * 保存
     */
    @SysLog("新增维修")
    @RequestMapping("/save")
    @RequiresPermissions("generator:pfacfix:save")
    public R save(@RequestBody PFacFixEntity pFacFix){
		pFacFixService.save(pFacFix);
        pFacDao.tosavefix(pFacFix.getFacNum());
        return R.ok();
    }

    /**
     * 修改
     */
    @SysLog("维修完成")
    @RequestMapping("/update")
    @RequiresPermissions("generator:pfacfix:update")
    public R update(@RequestBody PFacFixEntity pFacFix){
		pFacFixService.updateById(pFacFix);
        pFacDao.tosavefixok(pFacFix.getFacNum());
        return R.ok();
    }

    /**
     * 删除
     */
    @SysLog("删除维修记录")
    @RequestMapping("/delete")
    @RequiresPermissions("generator:pfacfix:delete")
    public R delete(@RequestBody Long[] ids){
		pFacFixService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }



    /**
     * 一条维修记录列表
     */
    @RequestMapping("/fixlist")
    @RequiresPermissions("generator:pfacfix:list")
    public R fixlist(@RequestParam Map<String, Object> params){
        PageUtils page = pFacFixService.queryPageFixlist(params);

        return R.ok().put("page", page);
    }
    /**
     * 一条维修记录列表
     */
    @RequestMapping("/passaudit/{id}")
    @RequiresPermissions("generator:pfacfix:audit")
    public R passaudit(@PathVariable("id") Long id,
                       @RequestParam Map<String, Object> params){
        //维修审核改状态
        pFacDao.changeToUseTo0byId(id.toString());
        facFixDao.changeStasTo0(params.get("facfix").toString());
        return R.ok();

    }


}
