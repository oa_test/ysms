package com.jesse.modules.generator.controller;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.jesse.common.utils.poi.ExcelUtil;
import com.jesse.modules.generator.dao.PFacCheckDao;
import com.jesse.modules.generator.entity.PFacEntity;
import com.jesse.modules.generator.entity.PFacOutEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jesse.modules.generator.entity.PFacCheckEntity;
import com.jesse.modules.generator.service.PFacCheckService;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;



/**
 *
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-10 23:25:30
 */
@RestController
@RequestMapping("generator/pfaccheck")
public class PFacCheckController {
    @Autowired
    private PFacCheckService pFacCheckService;
    @Autowired
    private PFacCheckDao checkDao;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:pfaccheck:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pFacCheckService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pfaccheck:info")
    public R info(@PathVariable("id") Long id){
		PFacCheckEntity pFacCheck = pFacCheckService.getById(id);

        return R.ok().put("pFacCheck", pFacCheck);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:pfaccheck:save")
    public R save(@RequestBody PFacCheckEntity pFacCheck){
		pFacCheckService.save(pFacCheck);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:pfaccheck:update")
    public R update(@RequestBody PFacCheckEntity pFacCheck){
		pFacCheckService.updateById(pFacCheck);
        return R.ok();
    }
    /**
     * 检查
     */
    @RequestMapping("/check")
    @RequiresPermissions("generator:pfaccheck:update")
    public R check(@RequestBody PFacCheckEntity pFacCheck) throws ParseException {
        pFacCheckService.check(pFacCheck);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:pfaccheck:delete")
    public R delete(@RequestBody Long[] ids){
		pFacCheckService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    @GetMapping("/export")
    public R export(PFacEntity pFacEntity)
    {
        List<PFacCheckEntity> list = checkDao.selectList(null);
        ExcelUtil<PFacCheckEntity> util = new ExcelUtil<PFacCheckEntity>(PFacCheckEntity.class);
        return util.exportExcel(list, "pFacEntity");
    }

}
