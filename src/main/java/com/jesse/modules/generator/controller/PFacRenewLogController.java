//package com.jesse.modules.generator.controller;
//
//import java.util.Arrays;
//import java.util.Map;
//
//import org.apache.shiro.authz.annotation.RequiresPermissions;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.jesse.modules.generator.entity.PFacRenewLogEntity;
//import com.jesse.modules.generator.service.PFacRenewLogService;
//import com.jesse.common.utils.PageUtils;
//import com.jesse.common.utils.R;
//
//
//
///**
// *
// *
// * @author chenshun
// * @email sunlightcs@gmail.com
// * @date 2021-03-27 17:17:28
// */
//@RestController
//@RequestMapping("generator/pfacrenewlog")
//public class PFacRenewLogController {
//    @Autowired
//    private PFacRenewLogService pFacRenewLogService;
//
//    /**
//     * 列表
//     */
//    @RequestMapping("/list")
//    @RequiresPermissions("generator:pfacout:list")
//    public R list(@RequestParam Map<String, Object> params){
//        PageUtils page = pFacRenewLogService.queryPage(params);
//
//        return R.ok().put("page", page);
//    }
//
//
//    /**
//     * 信息
//     */
//    @RequestMapping("/info/{id}")
//    @RequiresPermissions("generator:pfacrenewlog:info")
//    public R info(@PathVariable("id") Integer id){
//		PFacRenewLogEntity pFacRenewLog = pFacRenewLogService.getById(id);
//
//        return R.ok().put("pFacRenewLog", pFacRenewLog);
//    }
//
//    /**
//     * 保存
//     */
//    @RequestMapping("/save")
//    @RequiresPermissions("generator:pfacrenewlog:save")
//    public R save(@RequestBody PFacRenewLogEntity pFacRenewLog){
//		pFacRenewLogService.save(pFacRenewLog);
//
//        return R.ok();
//    }
//
//    /**
//     * 修改
//     */
//    @RequestMapping("/update")
//    @RequiresPermissions("generator:pfacrenewlog:update")
//    public R update(@RequestBody PFacRenewLogEntity pFacRenewLog){
//		pFacRenewLogService.updateById(pFacRenewLog);
//
//        return R.ok();
//    }
//
//    /**
//     * 删除
//     */
//    @RequestMapping("/delete")
//    @RequiresPermissions("generator:pfacrenewlog:delete")
//    public R delete(@RequestBody Integer[] ids){
//		pFacRenewLogService.removeByIds(Arrays.asList(ids));
//
//        return R.ok();
//    }
//
//}
