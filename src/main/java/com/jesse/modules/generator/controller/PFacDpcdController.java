package com.jesse.modules.generator.controller;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.jesse.common.annotation.SysLog;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.R;
import com.jesse.modules.generator.dao.PFacDao;
import com.jesse.modules.generator.entity.PFacDpcdEntity;
import com.jesse.modules.generator.service.PFacDpcdService;
import com.jesse.modules.generator.service.PFacRecycLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 设备报废登记表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@RestController
@RequestMapping("generator/pfacdpcd")
public class PFacDpcdController {
    @Autowired
    private PFacDpcdService pFacDpcdService;
    @Autowired
    private PFacDao pFacDao;
    @Autowired
    private PFacRecycLogService pFacRecycLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:pfacdpcd:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = pFacDpcdService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("generator:pfacdpcd:info")
    public R info(@PathVariable("id") Long id){
		PFacDpcdEntity pFacDpcd = pFacDpcdService.getById(id);

        return R.ok().put("pFacDpcd", pFacDpcd);
    }

    /**
     * 保存
     */
    @SysLog("设备报废")
    @RequestMapping("/save")
    @RequiresPermissions("generator:pfacdpcd:save")
    public R save(@RequestBody PFacDpcdEntity pFacDpcd){
        pFacDpcd.setStatus(3);
        pFacDpcd.setDpcdTime(new Timestamp(new Date().getTime()));
		pFacDpcdService.save(pFacDpcd);
        pFacDao.toOut(pFacDpcd.getFacNum());
        return R.ok();
    }

    @SysLog("设备报废再回收")
    @RequestMapping("/recycle")
    @RequiresPermissions("generator:pfacdpcd:save")
    public R recycle(@RequestBody Map<String,String> param){
        int res = pFacDpcdService.recycle(param);
        if(res==0){
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @SysLog("修改报废设备信息")
    @RequestMapping("/update")
    @RequiresPermissions("generator:pfacdpcd:update")
    public R update(@RequestBody PFacDpcdEntity pFacDpcd){
		pFacDpcdService.updateById(pFacDpcd);
        return R.ok();
    }

    /**
     * 删除
     */
    @SysLog("删除报废记录")
    @RequestMapping("/delete")
    @RequiresPermissions("generator:pfacdpcd:delete")
    public R delete(@RequestBody Long[] ids){
		pFacDpcdService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

    /**
     * 回收记录列表
     */
    @RequestMapping("/getCyclogListAll")
    @RequiresPermissions("generator:pfacdpcd:list")
    public R getCyclogListAll(@RequestParam Map<String, Object> params){
        PageUtils page = pFacRecycLogService.queryPage(params);

        return R.ok().put("page", page);
    }

}
