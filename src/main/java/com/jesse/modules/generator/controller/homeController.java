package com.jesse.modules.generator.controller;

import com.jesse.modules.generator.service.HomeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("generator/home")
public class homeController {
    @Autowired
    private HomeService homeService;
    /**
     * 查询首页 所有仪器状态 管理员
     */
    @RequestMapping("/allstatuslist")
    @ResponseBody
    @RequiresPermissions("generator:pfac:list")
    public List allstatuslist(@RequestParam Map<String, Object> params){
        List list = homeService.allstatuslist();
        return list;
    }
    /**
     * 查询首页 普通用户
     * @return
     */
    @RequestMapping("/statuslist")
    @ResponseBody
    @RequiresPermissions("generator:pfac:list")
    public List statuslist(@RequestParam Map<String, Object> params){
        List list = homeService.statuslist(params);
        return list;
    }

    @RequestMapping("/hotlist")
    @ResponseBody
    @RequiresPermissions("generator:pfac:list")
    public Map<String, ArrayList> hotFac(@RequestParam Map<String, Object> params){
        Map<String, ArrayList> map= homeService.hotList(params);
        return map;
    }


}
