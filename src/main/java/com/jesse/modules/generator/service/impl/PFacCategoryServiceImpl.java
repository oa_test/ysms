package com.jesse.modules.generator.service.impl;

import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;
import com.jesse.modules.generator.dao.PFacCategoryDao;
import com.jesse.modules.generator.entity.PFacCategoryEntity;
import com.jesse.modules.generator.service.PFacCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("pFacCategoryService")
public class PFacCategoryServiceImpl extends ServiceImpl<PFacCategoryDao, PFacCategoryEntity> implements PFacCategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PFacCategoryEntity> page = this.page(
                new Query<PFacCategoryEntity>().getPage(params),
                new QueryWrapper<PFacCategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<PFacCategoryEntity> listWithTree() {
        //1.查出所有分类
        List<PFacCategoryEntity> entities = baseMapper.selectList(null);

        //2.找出所有父子分类
        List<PFacCategoryEntity> level1Menus = entities.stream().filter(categoryEntity ->
                categoryEntity.getPid()==0).map((menu)-> {
            menu.setChildren(getChildrens(menu,entities));
            return menu;
        }).sorted((menu1,menu2)->{
            return (menu1.getSort())==null?0:menu1.getSort() - (menu2.getSort()==null?0:menu2.getSort());
        }).collect(Collectors.toList());

        return level1Menus;
    }

    private List<PFacCategoryEntity> getChildrens(PFacCategoryEntity root,List<PFacCategoryEntity> all){
        List<PFacCategoryEntity> children = all.stream().filter(categoryEntity->{
            return categoryEntity.getPid()==root.getId();
        }).map(categoryEntity->{
            categoryEntity.setChildren(getChildrens(categoryEntity,all));
            return categoryEntity;
        }).sorted((menu1,menu2)->{
            return (menu1.getSort())==null?0:menu1.getSort() - (menu2.getSort()==null?0:menu2.getSort());
        }).collect(Collectors.toList());
        return children;
    }

}
