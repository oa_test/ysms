package com.jesse.modules.generator.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;

import com.jesse.modules.generator.dao.PLiveDao;
import com.jesse.modules.generator.entity.PLiveEntity;
import com.jesse.modules.generator.service.PLiveService;


@Service("pLiveService")
public class PLiveServiceImpl extends ServiceImpl<PLiveDao, PLiveEntity> implements PLiveService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PLiveEntity> page = this.page(
                new Query<PLiveEntity>().getPage(params),
                new QueryWrapper<PLiveEntity>()
        );
        return new PageUtils(page);
    }
}
