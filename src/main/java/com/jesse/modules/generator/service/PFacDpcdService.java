package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacDpcdEntity;

import java.util.Map;

/**
 * 设备报废登记表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
public interface PFacDpcdService extends IService<PFacDpcdEntity> {

    PageUtils queryPage(Map<String, Object> params);

    int recycle(Map<String, String> param);
}

