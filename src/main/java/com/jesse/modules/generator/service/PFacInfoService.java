package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacInfoEntity;

import java.util.Map;

/**
 * 设备资料
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
public interface PFacInfoService extends IService<PFacInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);


}

