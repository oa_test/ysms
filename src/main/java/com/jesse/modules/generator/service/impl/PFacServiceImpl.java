package com.jesse.modules.generator.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;
import com.jesse.modules.generator.dao.PFacCheckDao;
import com.jesse.modules.generator.dao.PFacDao;
import com.jesse.modules.generator.entity.PFacCheckEntity;
import com.jesse.modules.generator.entity.PFacEntity;
import com.jesse.modules.generator.service.PFacService;
import com.jesse.modules.generator.vo.PFacVO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;


@Service("pFacService")
public class PFacServiceImpl extends ServiceImpl<PFacDao, PFacEntity> implements PFacService {
    @Autowired
    private PFacDao pFacDao;
    @Autowired
    private PFacCheckDao facCheckDao;

    @Autowired
    private PFacCheckDao checkDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        QueryWrapper<PFacEntity> wrapper = new QueryWrapper<>();

        if (!StringUtils.isBlank(params.get("type").toString())) {
            wrapper.eq("type", params.get("type").toString());
        }
        if (!StringUtils.isBlank(params.get("status").toString())) {
            wrapper.eq("status", params.get("status").toString());
        }
        if (!StringUtils.isBlank(params.get("name").toString())) {
            wrapper.like("name", params.get("name").toString());
        }
        if (!StringUtils.isBlank(params.get("categorys").toString())) {
            String[] split = params.get("categorys").toString().split(",");
            if (split.length == 1) {
                wrapper.eq("one_category_id", Long.parseLong(split[0]));
            }
            if (split.length == 2) {
                wrapper.eq("one_category_id", Long.parseLong(split[0]));
                wrapper.eq("two_category_id", Long.parseLong(split[1]));
            }
            if (split.length == 3) {
                wrapper.eq("one_category_id", Long.parseLong(split[0]));
                wrapper.eq("two_category_id", Long.parseLong(split[1]));
                wrapper.eq("three_category_id", Long.parseLong(split[2]));
            }
        }
        IPage<PFacEntity> page = this.page(
                new Query<PFacEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

    @Override
    public Page<PFacEntity> facVOList(Integer pageNum, Integer pageSize, PFacVO pFacVO) {
        Page<PFacEntity> page = new Page<>(pageNum,pageSize);
        QueryWrapper<PFacEntity> wrapper = new QueryWrapper<>();

        if(pFacVO.getThreeCategoryId()!=null) {
            wrapper.eq("one_category_id", pFacVO.getOneCategoryId());
            wrapper.eq("two_category_id", pFacVO.getTwoCategoryId());
            wrapper.eq("three_category_id", pFacVO.getThreeCategoryId());

            Page<PFacEntity> result = pFacDao.selectPage(page,wrapper);
            result.getRecords().forEach(System.out::println);
            return result;
        }

        if(pFacVO.getTwoCategoryId()!=null){
            wrapper.eq("one_category_id", pFacVO.getOneCategoryId());
            wrapper.eq("two_category_id", pFacVO.getTwoCategoryId());
            Page<PFacEntity> result = pFacDao.selectPage(page,wrapper);
            result.getRecords().forEach(System.out::println);
            return result;
        }

        if(pFacVO.getOneCategoryId()!=null){
            wrapper.eq("one_category_id", pFacVO.getOneCategoryId());
            Page<PFacEntity> result = pFacDao.selectPage(page,wrapper);
            result.getRecords().forEach(System.out::println);
            return result;
        }
        if (pFacVO.getName() != null && !"".equals(pFacVO.getName())) {

            wrapper.like(pFacVO.getName() != null, "name", pFacVO.getName());

            Page<PFacEntity> result = pFacDao.selectPage(page,wrapper);
            result.getRecords().forEach(System.out::println);
            return result;
        }
        return null;
    }

    @Override
    public PFacEntity isExist(PFacEntity pFac) {

        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("name",pFac.getName());
        PFacEntity pFacEntity = pFacDao.selectOne(wrapper);
        return pFacEntity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveFac(Map<String, Object> params) throws ParseException {
        String  strFac =  JSONArray.toJSONString(params.get("pFac"));
        String  strFacCheck =  JSONArray.toJSONString(params.get("pFacCheck"));

        PFacEntity facEntity = JSON.parseObject(strFac,PFacEntity.class);
        PFacCheckEntity facCheckEntity = JSON.parseObject(strFacCheck,PFacCheckEntity.class);

        facEntity.setStatus(0);
        facCheckEntity.setFacNum(facEntity.getFacNum());
        facCheckEntity.setName(facEntity.getName());
        facCheckEntity.setLastCheckDate(new Timestamp(new Date().getTime()));
        int a = pFacDao.insert(facEntity);
        int b = facCheckDao.insert(facCheckEntity);
        if (a!=1 || b!=1){
            throw new RuntimeException("pFacDao插入失败");
        }

        //
        //上次检查时间LastcheckTime
        Date LastcheckTime = facCheckEntity.getLastCheckDate();//取上次检查时间
        //下次检查时间NextcheckTime
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(LastcheckTime);
        calendar.add(calendar.DATE,facCheckEntity.getCycDays()); //把日期往后增加一天,整数  往后推,负数往前移动
        Date NextcheckTime=calendar.getTime(); //这个时间就是日期计算后的结果

        SimpleDateFormat formatTime = new SimpleDateFormat("YYYY-MM-dd");

        String time2 = formatTime.format(NextcheckTime);
        //今天的日期
        String nowTime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long m = sdf.parse(time2).getTime() - sdf.parse(nowTime).getTime();

        long remDays = ( m / (1000 * 60 * 60 * 24) );
        Integer remDays1 = Integer.parseInt(String.valueOf(remDays));


        facCheckEntity.setNextCheckTime(NextcheckTime);
        facCheckEntity.setRemDays(remDays1);
        //
        checkDao.updateById(facCheckEntity);


        return 0;
    }


    /**
     * 封装查询条件
     * @param categorys
     * @param pFacVO
     */
    private void buildCategorySearch(@RequestParam(value = "categorys", required = false) String categorys, PFacVO pFacVO) {
        if (categorys != null && !"".equals(categorys)) {
            String[] split = categorys.split(",");
            switch (split.length) {
                case 1:
                    pFacVO.setOneCategoryId(Long.parseLong(split[0]));
                    break;
                case 2:
                    pFacVO.setOneCategoryId(Long.parseLong(split[0]));
                    pFacVO.setTwoCategoryId(Long.parseLong(split[1]));
                    break;
                case 3:
                    pFacVO.setOneCategoryId(Long.parseLong(split[0]));
                    pFacVO.setTwoCategoryId(Long.parseLong(split[1]));
                    pFacVO.setThreeCategoryId(Long.parseLong(split[2]));
                    break;
            }
        }
    }
}
