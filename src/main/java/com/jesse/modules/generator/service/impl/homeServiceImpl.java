package com.jesse.modules.generator.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jesse.modules.generator.dao.PFacDao;
import com.jesse.modules.generator.dao.PFacFixDao;
import com.jesse.modules.generator.dao.PFacOutDao;
import com.jesse.modules.generator.entity.PFacEntity;
import com.jesse.modules.generator.entity.PFacOutEntity;
import com.jesse.modules.generator.vo.facDateVO;
import com.jesse.modules.generator.vo.facStatusVO;
import com.jesse.common.utils.RedisUtils;
import com.jesse.modules.generator.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service("homeService")
public class homeServiceImpl implements HomeService {
    @Autowired
    private PFacDao pFacDao;
    @Autowired
    private PFacOutDao facOutDao;
    @Autowired
    private PFacFixDao facFixDao;
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public List allstatuslist() {
        List<facStatusVO> page = pFacDao.statuslist();
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.DATE, -7);
        String month=(ca.get(Calendar.MONTH)+1)+"";
        String day=(ca.get(Calendar.DATE))+"";
        if(Integer.parseInt(month)<10){
            month="0"+month;
        }
        if(Integer.parseInt(day)<10){
            day="0"+day;
        }
        String time = ca.get(Calendar.YEAR)+(month)+day;
        List<facDateVO> list = facOutDao.select7day(time);
        List<facDateVO> listfix = facFixDao.select7day(time);
        int[] arr= new int[7];
        int[] arrfix= new int[7];
        for (int i=0;i<7;i++){
            int j = i;
            String finalTime = time;
            List<facDateVO> li = list.stream().filter(
                    s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(finalTime)+ j)) ).collect(Collectors.toList());
            arr[i]=li.size();

            String finalTime1 = time;
            List<facDateVO> li2 = listfix.stream().filter(
                    s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(finalTime1)+ j)) ).collect(Collectors.toList());
            arrfix[i]=li2.size();
        }
        int indexLine[] = arr;
        int indexLinefix[] = arrfix;

        List list2 = new LinkedList();
        list2.add(page);
        list2.add(indexLine);
        list2.add(indexLinefix);
        return list2;
    }
    @Override
    public List statuslist(Map<String, Object> params) {
        //总设备数
        int a = pFacDao.selectCount(null);
        //在借中(包括审核)
        String username = params.get("user").toString();
        QueryWrapper<PFacOutEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("user",username);
        wrapper.gt("status","0");
        List<PFacOutEntity> outlist = facOutDao.selectList(wrapper);
        Map<String,Integer> map = new HashMap();
        map.put("s",a);
        map.put("zj",outlist.size());
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.DATE, -7);
        String month=(ca.get(Calendar.MONTH)+1)+"";
        String day=(ca.get(Calendar.DATE))+"";
        if(Integer.parseInt(month)<10){
            month="0"+month;
        }
        if(Integer.parseInt(day)<10){
            day="0"+day;
        }
        String time = ca.get(Calendar.YEAR)+(month)+day;
        List<facDateVO> list = facOutDao.select7day(time);
        List<facDateVO> listfix = facFixDao.select7day(time);
        int[] arr= new int[7];
        int[] arrfix= new int[7];
        for (int i=0;i<7;i++){
            int j = i;
            String finalTime = time;
            List<facDateVO> li = list.stream().filter(
                    s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(finalTime)+ j)) ).collect(Collectors.toList());
            arr[i]=li.size();

            String finalTime1 = time;
            List<facDateVO> li2 = listfix.stream().filter(
                    s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(finalTime1)+ j)) ).collect(Collectors.toList());
            arrfix[i]=li2.size();
        }
        int indexLine[] = arr;
        int indexLinefix[] = arrfix;
        List list3 = new LinkedList();
        list3.add(map);
        list3.add(indexLine);
        list3.add(indexLinefix);
        return list3;
    }

    @Override
    public Map<String, ArrayList> hotList(Map<String, Object> params) {
        List<PFacEntity> list = pFacDao.getHotList();
        Map map = new HashMap();
        List<String> name = new ArrayList<>();
        List<Integer> count = new ArrayList<>();

        for (PFacEntity fac: list) {
            name.add(fac.getName());
            count.add(fac.getUseCount());
        }

        map.put("name",name);
        map.put("count",count);
        return map;
    }
}
