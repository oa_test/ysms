package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PLiveEntity;

import java.util.Map;

/**
 * 直播
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-03-16 11:03:22
 */
public interface PLiveService extends IService<PLiveEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

