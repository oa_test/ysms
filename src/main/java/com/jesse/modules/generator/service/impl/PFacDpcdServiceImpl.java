package com.jesse.modules.generator.service.impl;

import com.jesse.modules.generator.dao.PFacDao;
import com.jesse.modules.generator.dao.PFacRecycLogDao;
import com.jesse.modules.generator.entity.PFacEntity;
import com.jesse.modules.generator.entity.PFacRecycLogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;

import com.jesse.modules.generator.dao.PFacDpcdDao;
import com.jesse.modules.generator.entity.PFacDpcdEntity;
import com.jesse.modules.generator.service.PFacDpcdService;


@Service("pFacDpcdService")
public class PFacDpcdServiceImpl extends ServiceImpl<PFacDpcdDao, PFacDpcdEntity> implements PFacDpcdService {

    @Autowired
    private PFacDao facDao;
    @Autowired
    private PFacRecycLogDao facRecycLogDao;
    @Autowired
    private PFacDpcdDao pFacDpcdDao;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PFacDpcdEntity> page = this.page(
                new Query<PFacDpcdEntity>().getPage(params),
                new QueryWrapper<PFacDpcdEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public int recycle(Map<String, String> param) {

        PFacEntity facEntity = facDao.selectIsDeletedByNum(param.get("facNum"));

        if(facEntity!=null){
            facDao.changeToNotDeleted(param.get("facNum"));
            pFacDpcdDao.changeStatusToRecycle(param.get("facNum"));
        }

        PFacRecycLogEntity facRecycLogEntity = new PFacRecycLogEntity();
        facRecycLogEntity.setFacNum(param.get("facNum"));
        facRecycLogEntity.setRecycRemark(param.get("remark"));
        facRecycLogEntity.setUser(param.get("user"));
        facRecycLogEntity.setImageUrl(facEntity.getImageUrl());
        facRecycLogDao.insert(facRecycLogEntity);

        return 0;
    }

}
