package com.jesse.modules.generator.service.impl;

import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;
import com.jesse.modules.generator.dao.PFacInfoDao;
import com.jesse.modules.generator.entity.PFacInfoEntity;
import com.jesse.modules.generator.service.PFacInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("pFacInfoService")
public class PFacInfoServiceImpl extends ServiceImpl<PFacInfoDao, PFacInfoEntity> implements PFacInfoService {



    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<PFacInfoEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isBlank(params.get("key").toString())){
            wrapper.eq("name",params.get("key"));
        }

        IPage<PFacInfoEntity> page = this.page(
                new Query<PFacInfoEntity>().getPage(params), wrapper
        );

        return new PageUtils(page);
    }

}
