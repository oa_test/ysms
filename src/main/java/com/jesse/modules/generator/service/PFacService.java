package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacEntity;
import com.jesse.modules.generator.vo.PFacVO;

import java.text.ParseException;
import java.util.Map;

/**
 * 设备详情
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
public interface PFacService extends IService<PFacEntity> {

    PageUtils queryPage(Map<String, Object> params);
    Page<PFacEntity> facVOList(Integer pageNum, Integer pageSize, PFacVO pFacVO);
    PFacEntity isExist(PFacEntity pFac);

    int saveFac(Map<String,Object> params) throws ParseException;
}

