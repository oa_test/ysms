package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacFixEntity;

import java.util.Map;

/**
 * 设备维修记录
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
public interface PFacFixService extends IService<PFacFixEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageFixlist(Map<String, Object> params);
}

