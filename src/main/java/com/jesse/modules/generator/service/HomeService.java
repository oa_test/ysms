package com.jesse.modules.generator.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface HomeService {
     List allstatuslist();
     List statuslist(Map<String, Object> params);

    Map<String, ArrayList> hotList(Map<String, Object> params);
}
