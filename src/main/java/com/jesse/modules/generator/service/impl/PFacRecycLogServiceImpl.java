package com.jesse.modules.generator.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;

import com.jesse.modules.generator.dao.PFacRecycLogDao;
import com.jesse.modules.generator.entity.PFacRecycLogEntity;
import com.jesse.modules.generator.service.PFacRecycLogService;


@Service("pFacRecycLogService")
public class PFacRecycLogServiceImpl extends ServiceImpl<PFacRecycLogDao, PFacRecycLogEntity> implements PFacRecycLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PFacRecycLogEntity> page = this.page(
                new Query<PFacRecycLogEntity>().getPage(params),
                new QueryWrapper<PFacRecycLogEntity>()
        );

        return new PageUtils(page);
    }

}
