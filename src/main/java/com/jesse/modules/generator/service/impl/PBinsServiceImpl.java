package com.jesse.modules.generator.service.impl;

import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;
import com.jesse.modules.generator.dao.PBinsDao;
import com.jesse.modules.generator.entity.PBinsEntity;
import com.jesse.modules.generator.service.PBinsService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("pBinsService")
public class PBinsServiceImpl extends ServiceImpl<PBinsDao, PBinsEntity> implements PBinsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PBinsEntity> page = this.page(
                new Query<PBinsEntity>().getPage(params),
                new QueryWrapper<PBinsEntity>()
        );

        return new PageUtils(page);
    }

}
