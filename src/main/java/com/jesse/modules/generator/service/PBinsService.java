package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PBinsEntity;

import java.util.Map;

/**
 * 生产厂家信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
public interface PBinsService extends IService<PBinsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

