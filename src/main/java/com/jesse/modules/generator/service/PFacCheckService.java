package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacCheckEntity;

import java.text.ParseException;
import java.util.Map;

/**
 *
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-10 23:25:30
 */
public interface PFacCheckService extends IService<PFacCheckEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void check(PFacCheckEntity pFacCheck) throws ParseException;
}

