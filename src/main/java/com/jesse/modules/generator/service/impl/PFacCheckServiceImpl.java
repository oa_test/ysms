package com.jesse.modules.generator.service.impl;

import com.jesse.modules.generator.entity.PFacInfoEntity;
import com.jesse.modules.generator.entity.PFacOutEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;

import com.jesse.modules.generator.dao.PFacCheckDao;
import com.jesse.modules.generator.entity.PFacCheckEntity;
import com.jesse.modules.generator.service.PFacCheckService;


@Service("pFacCheckService")
public class PFacCheckServiceImpl extends ServiceImpl<PFacCheckDao, PFacCheckEntity> implements PFacCheckService {

    @Autowired
    private PFacCheckDao checkDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String name = (String)params.get("key");
        QueryWrapper<PFacCheckEntity> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("rem_days");
        if(StringUtils.isNotBlank(name)){
            wrapper.like("name", name);
        }

        IPage<PFacCheckEntity> page = this.page(
                new Query<PFacCheckEntity>().getPage(params),wrapper);

        return new PageUtils(page);
    }

    @Override
    public void check(PFacCheckEntity pFacCheck) throws ParseException {
//        System.out.println(pFacCheck.toString()+"**1");
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("fac_num",pFacCheck.getFacNum());
        PFacCheckEntity facCheckEntity = checkDao.selectOne(wrapper);
//        System.out.println(facCheckEntity.toString()+"**2");
        facCheckEntity.setName(pFacCheck.getName());
        facCheckEntity.setLastCheckDate(new Timestamp(new Date().getTime()));
        facCheckEntity.setRemark(pFacCheck.getRemark());

        //
        //上次检查时间LastcheckTime
        Date LastcheckTime = facCheckEntity.getLastCheckDate();//取上次检查时间
        //下次检查时间NextcheckTime
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(LastcheckTime);
        calendar.add(calendar.DATE,facCheckEntity.getCycDays()); //把日期往后增加一天,整数  往后推,负数往前移动
        Date NextcheckTime=calendar.getTime(); //这个时间就是日期计算后的结果

        SimpleDateFormat formatTime = new SimpleDateFormat("YYYY-MM-dd");

        String time2 = formatTime.format(NextcheckTime);
        //今天的日期
        String nowTime = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long m = sdf.parse(time2).getTime() - sdf.parse(nowTime).getTime();

        long remDays = ( m / (1000 * 60 * 60 * 24) );
        Integer remDays1 = Integer.parseInt(String.valueOf(remDays));


        facCheckEntity.setNextCheckTime(NextcheckTime);
        facCheckEntity.setRemDays(remDays1);
        //
        checkDao.updateById(facCheckEntity);
//        System.out.println(facCheckEntity.toString()+"**3");
    }
}
