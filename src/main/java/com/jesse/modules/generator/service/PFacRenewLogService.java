package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacRenewLogEntity;

import java.util.Map;

/**
 *
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-03-27 17:17:28
 */
public interface PFacRenewLogService extends IService<PFacRenewLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils getRenewListAll(Map<String, Object> params);

    PageUtils getRenewListUser(Map<String, Object> params);
}

