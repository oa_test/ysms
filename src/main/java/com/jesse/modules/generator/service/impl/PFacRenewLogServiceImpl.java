package com.jesse.modules.generator.service.impl;

import com.jesse.modules.generator.dao.PFacRenewLogDao;
import com.jesse.modules.generator.entity.PFacOutEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;
import com.jesse.modules.generator.entity.PFacRenewLogEntity;
import com.jesse.modules.generator.service.PFacRenewLogService;


@Service("pFacRenewLogService")
public class PFacRenewLogServiceImpl extends ServiceImpl<PFacRenewLogDao, PFacRenewLogEntity> implements PFacRenewLogService {


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        return null;
    }

    @Override
    public PageUtils getRenewListAll(Map<String, Object> params) {
        QueryWrapper<PFacRenewLogEntity> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(params.get("key").toString())){
            wrapper.eq("fac_num",params.get("key").toString());
        }
        wrapper.orderByAsc("create_time");
        System.out.println(params.toString());
        IPage<PFacRenewLogEntity> page = this.page(
                new Query<PFacRenewLogEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

    @Override
    public PageUtils getRenewListUser(Map<String, Object> params) {
        QueryWrapper<PFacRenewLogEntity> wrapper = new QueryWrapper<>();
        if(StringUtils.isNotBlank(params.get("key").toString())){
            wrapper.eq("fac_num",params.get("key").toString());
        }
        if(StringUtils.isNotBlank(params.get("user").toString())){
            wrapper.eq("user",params.get("user").toString());
        }
        wrapper.orderByAsc("create_time");
        System.out.println(params.toString());
        IPage<PFacRenewLogEntity> page = this.page(
                new Query<PFacRenewLogEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

}
