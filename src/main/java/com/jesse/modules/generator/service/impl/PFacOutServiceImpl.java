package com.jesse.modules.generator.service.impl;

import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;
import com.jesse.modules.generator.dao.PFacOutDao;
import com.jesse.modules.generator.dao.PFacRenewLogDao;
import com.jesse.modules.generator.entity.PFacOutEntity;
import com.jesse.modules.generator.entity.PFacRenewLogEntity;
import com.jesse.modules.generator.service.PFacOutService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("pFacOutService")
public class PFacOutServiceImpl extends ServiceImpl<PFacOutDao, PFacOutEntity> implements PFacOutService {
    @Autowired
    private PFacOutDao facOutDao;
    @Autowired
    private PFacRenewLogDao facRenewDao;

    private Logger logger = LoggerFactory.getLogger(getClass());
    /*
    *查询全部
     */
    @Override
    public PageUtils aQueryPage(Map<String, Object> params) {

        String fac_num = (String)params.get("key");

        QueryWrapper<PFacOutEntity> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("create_time");

        if(StringUtils.isNotBlank(fac_num)){
            wrapper.eq("fac_num",fac_num);
        }
        try {
            if(!StringUtils.isBlank(params.get("status").toString())){
                wrapper.eq("status",params.get("status"));
            }
        }catch(Exception e){
            logger.debug("在查询设备调度页面 没status参数-->"+e);
        }
        if(!StringUtils.isBlank(params.get("user").toString())){
            wrapper.eq("user",params.get("user"));
        }
        IPage<PFacOutEntity> page = this.page(
                new Query<PFacOutEntity>().getPage(params), wrapper
        );
        return new PageUtils(page);
    }
    /*
     *查询某个用户的,params.get("user")报错
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        QueryWrapper<PFacOutEntity> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("create_time");

        if(StringUtils.isNotBlank(params.get("key").toString())){
            wrapper.eq("fac_num",params.get("key"));
        }
        try {
            if(!StringUtils.isBlank(params.get("status").toString())){
                wrapper.eq("status",params.get("status"));
            }
        }catch(Exception e){
            logger.debug("queryPage在查询设备调度页面 没status参数-->"+e);
        }

        IPage<PFacOutEntity> page = this.page(
                new Query<PFacOutEntity>().getPage(params), wrapper);
        return new PageUtils(page);
    }

    @Override
    public void renewOutFac(PFacOutEntity pFacOut) {
        //续借
        //1.修改fac_out表归还时间
        facOutDao.updateByFacNum(pFacOut);
        //2.log表插入记录
        PFacRenewLogEntity renewLogEntity = new PFacRenewLogEntity();
        renewLogEntity.setFacNum(pFacOut.getFacNum());
        renewLogEntity.setNewTime(pFacOut.getRetnTime());
//        renewLogEntity.setOldTime(pFacOut.getNextTime1());
        renewLogEntity.setRenewRemark(pFacOut.getRemark());
        renewLogEntity.setUser(pFacOut.getUser());
        facRenewDao.insert(renewLogEntity);
    }

}
