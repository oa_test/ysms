package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacOutEntity;

import java.util.Map;

/**
 * 设备使用
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
public interface PFacOutService extends IService<PFacOutEntity> {

    PageUtils aQueryPage(Map<String, Object> params);
    PageUtils queryPage(Map<String, Object> params);

    void renewOutFac(PFacOutEntity pFacOut);


}

