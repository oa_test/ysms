package com.jesse.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jesse.common.utils.PageUtils;
import com.jesse.modules.generator.entity.PFacCategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 分类
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
public interface PFacCategoryService extends IService<PFacCategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<PFacCategoryEntity> listWithTree();
}

