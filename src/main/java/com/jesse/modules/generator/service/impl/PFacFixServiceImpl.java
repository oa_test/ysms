package com.jesse.modules.generator.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jesse.common.utils.PageUtils;
import com.jesse.common.utils.Query;

import com.jesse.modules.generator.dao.PFacFixDao;
import com.jesse.modules.generator.entity.PFacFixEntity;
import com.jesse.modules.generator.service.PFacFixService;


@Service("pFacFixService")
public class PFacFixServiceImpl extends ServiceImpl<PFacFixDao, PFacFixEntity> implements PFacFixService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String facNum = (String)params.get("facNum");
        IPage<PFacFixEntity> page = this.page(
                new Query<PFacFixEntity>().getPage(params),
                new QueryWrapper<PFacFixEntity>()
                        .like(StringUtils.isNotBlank(facNum),"fac_num", facNum)
        );
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageFixlist(Map<String, Object> params) {

        QueryWrapper<PFacFixEntity> wrapper = new QueryWrapper<>();
        if(!StringUtils.isBlank(params.get("status").toString())){
            wrapper.eq("status",params.get("status"));
        }
//        if(!StringUtils.isBlank(params.get("key").toString())){
//            wrapper.eq("fac_num",params.get("key"));
//        }
        if(params.containsKey("key")){
            wrapper.eq("fac_num",params.get("key"));
        }
        IPage<PFacFixEntity> page = this.page(
                new Query<PFacFixEntity>().getPage(params),wrapper
        );
        return new PageUtils(page);
    }

}
