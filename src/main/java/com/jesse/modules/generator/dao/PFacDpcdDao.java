package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacDpcdEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * 设备报废登记表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Mapper
public interface PFacDpcdDao extends BaseMapper<PFacDpcdEntity> {

    @Update("update p_fac_dpcd set status=4 where fac_num=#{facNum}")
    void changeStatusToRecycle(String facNum);
}
