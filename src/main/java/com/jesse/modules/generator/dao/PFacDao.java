package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.vo.facStatusVO;
import com.jesse.modules.generator.entity.PFacEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 设备详情
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Mapper
public interface PFacDao extends BaseMapper<PFacEntity> {
    @Select("update p_fac set `status`= 4 where fac_num=#{facNum}")
    void changeToUseTo4(String facNum);

    @Select("update p_fac set status = 0 where fac_num=#{facNum}")
    void changeToUseTo0(String facNum);

    @Select("update p_fac set status = 1 where id=#{id}")
    void passaudit(Long id);

//    @Select("update p_fac set status = 0  where id=#{id}")
//    void refusaudit(Long id);

    @Select("update p_fac set status = 7 where fac_num=#{facNum}")
    void changeToUseTo7(String facNum);

//    @Select("update p_fac set status = 0 where id=#{id}")
//    void passRtnAudit(Long id);

    @Select("update p_fac set status = 2 where fac_num=#{facNum}")
    void tosavefix(String facNum);
    @Select("update p_fac set status = 5 where fac_num=#{facNum}")
    void tosavefixok(String facNum);

    @Select("update p_fac set status = 0 where id=#{id}")
    void changeToUseTo0byId(String id);


    @Select("select \n" +
            "(select count(1) from p_fac) as s,\n" +
            "(select count(1) from p_fac pf where pf.status = 2 or pf.status = 5) as wx,\n" +
            "(select count(1)  from p_fac pf where pf.status = 1 or pf.status = 4 or pf.status = 7 ) as zj,\n" +
            "(select count(1)   from p_fac pf where pf.status = 3 or pf.status = 6) as bf,\n" +
            "(select count(1)  from p_fac pf where pf.status = 4 ) as ysh,\n" +
            "(select count(1)  from p_fac pf where pf.status = 5) as wsh,\n" +
            "(select count(1)  from p_fac pf where pf.status = 6 ) as bsh,\n" +
            "(select count(1)  from p_fac pf where pf.status = 7) as gsh\n" +
            ";")
    List<facStatusVO> statuslist();

    @Select("update p_fac set deleted = -1 where fac_num=#{facNum}")
    void toOut(String facNum);

    @Select("select * from p_fac where fac_num=#{facNum}")
    PFacEntity selectIsDeletedByNum(String facNum);

    @Update("update p_fac set deleted = 0 where fac_num=#{facNum}")
    void changeToNotDeleted(String facNum);
    @Select("select id,name,use_count from p_fac order BY use_count DESC limit 5")
    List<PFacEntity> getHotList();
}
