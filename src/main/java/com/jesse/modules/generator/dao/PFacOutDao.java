package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacOutEntity;
import com.jesse.modules.generator.vo.facDateVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 设备使用
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Mapper
public interface PFacOutDao extends BaseMapper<PFacOutEntity> {
    @Select("update p_fac_out set status = 0 where id=#{facNum}")
    void changeStasTo0(String facNum);

    @Select("update p_fac_out set status = 1 where id=#{facout}")
    void passaudit(String facout);
    @Select("update p_fac_out set status = 3 where id=#{facout}")
    void refusaudit(String facout);

    @Select("update p_fac_out set status = 4 where id=#{facNum}")
    void changeStasTo4(String facNum);
    @Select("update p_fac_out set status = 0 where id=#{facout}")
    void passRtnAudit(String facout);

    @Select("select fac_num,create_time from p_fac_out where date_format(create_time,'%Y%m%d') >=#{time}")
    List<facDateVO> select7day(String time);


    void updateByFacNum(PFacOutEntity pFacOut);
}
