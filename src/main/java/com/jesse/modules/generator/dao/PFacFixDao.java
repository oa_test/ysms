package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacFixEntity;
import com.jesse.modules.generator.vo.facDateVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 设备维修记录
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Mapper
public interface PFacFixDao extends BaseMapper<PFacFixEntity> {
    @Select("update p_fac_fix set status = 0 where id=#{id}")
    void changeStasTo0(String id);

    @Select("select fac_num,create_time from p_fac_fix where date_format(create_time,'%Y%m%d') >=#{time}")
    List<facDateVO> select7day(String time);
}
