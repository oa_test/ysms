package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PBinsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 生产厂家信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Mapper
public interface PBinsDao extends BaseMapper<PBinsEntity> {

}
