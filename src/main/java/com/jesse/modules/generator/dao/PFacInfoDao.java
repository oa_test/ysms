package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 设备资料
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Mapper
public interface PFacInfoDao extends BaseMapper<PFacInfoEntity> {

}
