package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacRenewLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-03-27 17:17:28
 */
@Mapper
public interface PFacRenewLogDao extends BaseMapper<PFacRenewLogEntity> {

}
