package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 分类
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-18 17:55:20
 */
@Mapper
public interface PFacCategoryDao extends BaseMapper<PFacCategoryEntity> {
    @Select("select * from p_fac_category where id in (#{one},#{two},#{three})")
    List<PFacCategoryEntity> get(int one, int two, int three);
}
