package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacCheckEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-04-10 23:25:30
 */
@Mapper
public interface PFacCheckDao extends BaseMapper<PFacCheckEntity> {

}
