package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PLiveEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 直播
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-03-16 11:03:22
 */
@Mapper
public interface PLiveDao extends BaseMapper<PLiveEntity> {

}
