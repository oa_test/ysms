package com.jesse.modules.generator.vo;

import lombok.Data;

import java.util.Date;

@Data
public class facDateVO {
    private String facNum;
    private Date createTime;
}
