package com.jesse.modules.generator.vo;

import lombok.Data;

@Data
public class facStatusVO {
    private int s;
    private int wx;
    private int zj;
    private int bf;
    private int ysh;
    private int wsh;
    private int bsh;
    private int gsh;
}
