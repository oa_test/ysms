package com.jesse.modules.generator.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class PFacVO {


    private Long id;
    /**
     * 编号
     */
    private String facNum;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 说明文档
     */
    private String guide;
    /**
     * 图片
     */
    private String imageUrl;
    /**
     * 类型:1采购，2捐赠
     */
    private Integer type;
    /**
     * 操作员
     */
    private String operator;
    /**
     * 规格型号
     */
    private String model;
    /**
     * 单位
     */
    private String unit;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 设备种类
     */
    private String kind;
    /**
     * 所属品牌
     */
    private String brand;
    /**
     * 在使用人
     */
    private String user;
    /**
     * 放置地点
     */
    private String address;
    /**
     * 使用次数
     */
    private Integer useCount;
    /**
     * 状态 0空闲 1在借 2维修 3报废
     */
    private Integer status;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date modifiedTime;
    /**
     * 购买部门
     */
    private String buyDep;
    /**
     * 生产厂家
     */
    private Integer made;
    /**
     * 产地
     */
    private String madeIn;
    /**
     * 出厂日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date madeOutTime;
    /**
     * 购买时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date buyTime;
    /**
     * 购买价格
     */
    private BigDecimal price;
    /**
     * 启用时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date enableTime;
    /**
     * 备注
     */
    private String remark;

    /**
     * 联系电话
     */
    private String phone;
    /**
     * 1级分类
     */
    private Long oneCategoryId;
    /**
     * 2级分类
     */
    private Long twoCategoryId;
    /**
     * 3级分类
     */
    private Long threeCategoryId;

    private Long[] categoryKeys;
}
