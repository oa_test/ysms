package com.jesse.modules.job;

import com.jesse.common.utils.RedisUtils;
import com.jesse.modules.generator.dao.PFacFixDao;
import com.jesse.modules.generator.dao.PFacOutDao;
import com.jesse.modules.generator.vo.facDateVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class redistest {
    @Autowired
    private PFacOutDao facOutDao;

    @Autowired
    private PFacFixDao facFixDao;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private RedisUtils redisUtils;

    @Test
    public void test(){
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.DATE, -7);
        String month=""+ca.get(Calendar.MONTH)+1;
        String time = ca.get(Calendar.YEAR)+""+(month)+""+ca.get(Calendar.DATE);
        List<facDateVO> list = facOutDao.select7day(time);
        List<facDateVO> listfix = facFixDao.select7day(time);
        int[] arr= new int[7];
        int[] arrfix= new int[7];
        for (int i=0;i<7;i++){
            int j = i;
            List<facDateVO> li = list.stream().filter(
                    s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(time)+ j)) ).collect(Collectors.toList());
            arr[i]=li.size();

            List<facDateVO> li2 = listfix.stream().filter(
                    s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(time)+ j)) ).collect(Collectors.toList());
            arrfix[i]=li2.size();
        }
        for(int a : arr){
            System.out.println(a);
        }
        for(int a : arrfix){
            System.out.println(a);
        }
        redisUtils.set("indexLine",arr);
        Map<String,Object> map = new HashMap<>();
        map.put("a",1);
        map.put("b",2);
        redisTemplate.boundHashOps("hashkey").put("1","2");
//        redisTemplate.boundHashOps("hashkey").
        redisUtils.set("indexLinefix",arrfix);
    }
}
