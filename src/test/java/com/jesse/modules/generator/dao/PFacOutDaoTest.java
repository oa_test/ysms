package com.jesse.modules.generator.dao;

import com.jesse.common.utils.RedisUtils;
import com.jesse.modules.generator.vo.facDateVO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public  class PFacOutDaoTest {

    @Autowired
    private PFacOutDao facOutDao;
    @Autowired
    private RedisUtils redisUtils;
    @Test
    public  void test(){
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.DATE, -7);
        String month=""+ca.get(Calendar.MONTH)+1;
        String time = ca.get(Calendar.YEAR)+""+(month)+""+ca.get(Calendar.DATE);
        List<facDateVO> list = facOutDao.select7day(time);
        int[] arr= new int[7];
        for (int i=0;i<7;i++){
            int j = i;
            List<facDateVO> li = list.stream().filter(
                    s-> new SimpleDateFormat("yyyyMMdd").format(s.getCreateTime()).equals(String.valueOf(Integer.parseInt(time)+ j)) ).collect(Collectors.toList());
            arr[i]=li.size();
        }
        for(int a : arr){
            System.out.println(a);
        }
//        redisTemplate.opsForValue().set("indexLine",arr);
        redisUtils.set("indexLine",arr);
    }
}
