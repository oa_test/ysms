package com.jesse.modules.generator.dao;

import com.jesse.modules.generator.entity.PFacCategoryEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class PFacCategoryDaoTest {
    @Autowired
    PFacCategoryDao categoryDao;

    @Test
    public void test(){
        List<PFacCategoryEntity> categoryList = categoryDao.get(7,108,114);
        categoryList.forEach(System.out::println);
    }

}
